let myHeading = document.querySelector('h1'); /*part of the DOM API*/ /*the resulting myHeading is an object*/
myHeading.textContent = 'Hello World!';/*part of the DOM API*/

document.querySelector('html').onclick = function() {
        alert('Ouch! Stop pocking me!');
}

let myImage = document.querySelector('img');
myImage.onclick = function() {
        let mySrc = myImage.getAttribute('src');
        if (mySrc === 'images/Homer11_1280.png') {
                myImage.setAttribute('src', 'images/Screenshot_2012-08-25-22-16-46.png');
        }
        else {
                myImage.setAttribute('src', 'images/Homer11_1280.png');
        }
}

let myButton = document.querySelector('button');

function setUserName() {
        let myName = prompt('Please enter your name.'); /* <-- doesn't work on edge*/
        localStorage.setItem('name', myName); /* <-- using the Web Storage API*/
        myHeading.textContent = 'Say Hello To My Little Friend, ' + myName;
}
if (!localStorage.getItem('name')) { /*init code. BTW, the API allows us to store data on the browser.
                                                        nb. it persidts after the site is closed down.*/
        setUserName();
}
else {
        let storedName = localStorage.getItem('name');
        myHeading.textContent = 'Say Hello To My Little Friend, ' + storedName;
}
myButton.onclick = function() {
        setUserName();
}